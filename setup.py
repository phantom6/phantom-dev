from setuptools import setup

from roboversion import get_version


with open('README.md') as readme:
	long_description = readme.read()


package_data = {
	'name': 'phantom-dev',
	'version': str(get_version()),
	'packages': [
		'phantom_dev.action_handler',
		'phantom_dev.app',
		'phantom_dev.client',
		'phantom_dev.dummy',
		'phantom_dev.dummy.phantom',
	],
	'author': 'David Finn',
	'author_email': 'dfinn@splunk.com',
	'classifiers': [
		'License :: OSI Approved :: MIT License',
		'Programming Language :: Python :: 3.6',
	],
	'description': 'Utilities to simplify Phantom app development',
	'entry_points': {
		'console_scripts': [
			'phantom-dev = phantom_dev.client:main',
		],
	},
	'install_requires': [
		'docstring-parser',
		'paramiko',
		'pytest',
		'pyyaml',
		'roboversion>=2',
		'sshtunnel>=0.4.0',
	],
	'keywords': ['splunk', 'phantom'],
	'long_description': long_description,
	'long_description_content_type': "text/markdown",
	'python_requires': '>=3.6',
	'package_data': {
		'phantom_dev.app': ['default_logo.svg'],
		'phantom_dev.client': ['default_data/*'],
	},
	'url': 'https://gitlab.com/phantom6/phantom-dev',
}

setup(**package_data)
