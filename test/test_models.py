
from datetime import datetime

from pytest import fixture

from phantom_dev.dummy import MockedPhantomModule

with MockedPhantomModule():
	from phantom_dev.action_handler.models import (
		Artifact, Container, datetime_data_member)


@fixture
def container_json():
	return {
		"id": 107,
		"version": "1",
		"label": "incident",
		"name": "my_test_incident",
		"source_data_identifier": "64c2a9a4-d6ef-4da8-ad6f-982d785f14b2",
		"description": "this is my test incident",
		"status": "open",
		"sensitivity": "amber",
		"severity": "medium",
		"create_time": "2016-01-16 07:18:46.631897+00",
		"start_time": "2016-01-16 07:18:46.636966+00",
		"end_time": "",
		"due_time": "2016-01-16 19:18:00+00",
		"close_time": "",
		"kill_chain": "",
		"owner": "admin",
		"hash": "52d277ed6eba51d86190cd72405df749",
		"tags": [""],
		"asset_name": "",
		"artifact_update_time": "2016-01-16 07:18:46.631875+00",
		"container_update_time": "2016-01-16 07:19:12.359376+00",
		"ingest_app_id": "",
		"data": {},
		"artifact_count": 8
	}


@fixture(params=['constructed', 'from_fields'])
def container(request, container_json):
	explicit_fields = dict(container_json)
	implicit_fields = {}
	for name in [
			'version',
			'create_time',
			'start_time',
			'end_time',
			'close_time',
			'hash',
			'asset_name',
			'artifact_update_time',
			'container_update_time',
			'ingest_app_id',
			'artifact_count',
	]:
		implicit_fields[name] = explicit_fields.pop(name)

	explicit_fields['due_time'] = datetime.strptime(
		explicit_fields['due_time'] + '00', Container.due_time.FORMAT)

	mapping = {
		'constructed': Container(container_json),
		'from_fields': Container.from_fields(**explicit_fields),
	}
	return mapping[request.param]


@fixture
def artifact_json():
	return {
		"id": 1,
		"version": 1,
		"name": "test",
		"label": "event",
		"source_data_identifier": "140a7ae0-9da5-4ee2-b06c-64faa313e94a",
		"create_time": "2016-01-18T19:26:39.053087Z",
		"start_time": "2016-01-18T19:26:39.058797Z",
		"end_time": None,
		"severity": "low",
		"type": None,
		"kill_chain": None,
		"hash": "7a61100894c1eb24a59c67ce245d2d8c",
		"cef": {
			"sourceAddress": "1.1.1.1"
		},
		"container": 1,
		"description": None,
		"tags": [""],
		"data": {}
	}

@fixture(params=['constructed', 'from_fields'])
def artifact(request, artifact_json):
	explicit_fields = dict(artifact_json)
	implicit_fields = {}
	for name in [
			'version',
			'create_time',
			'start_time',
			'end_time',
			'hash',
	]:
		implicit_fields[name] = explicit_fields.pop(name)
	
	mapping = {
		'constructed': Artifact(artifact_json),
		'from_fields': Artifact.from_fields(**explicit_fields)
	}
	return mapping[request.param]


def test_container(container_json, container: Container):
	for key, value in container.items():
		descriptor = getattr(Container, key)
		container_value = getattr(container, key)
		if descriptor.data_type is datetime:
			pass
		else:
			assert value == container_value

	for key, value in container_json.items():
		descriptor = getattr(Container, key)
		try:
			is_datetime = issubclass(descriptor.data_type, datetime)
		except TypeError:
			assert key in ['tags', 'data', 'kill_chain']
		else:
			if is_datetime:
				if not value:
					value = None
				else:
					if not value.endswith('Z'):
						value += '00'

					value = datetime.strptime(value, descriptor.FORMAT)

		setattr(container, key, value)
		descriptor = getattr(Container, key)
		container_value = getattr(container, key)
		if descriptor.data_type is datetime:
			pass
		elif container_value is None:
			assert value is None
		else:
			assert value == container_value

		if container_value is not None:
			try:
				assert isinstance(container_value, descriptor.data_type)
			except TypeError:
				assert key in ['tags', 'data', 'kill_chain']



def test_artifact(artifact_json, artifact: Artifact):
	for key, value in artifact.items():
		descriptor = getattr(Artifact, key)
		artifact_value = getattr(artifact, key)
		if descriptor.data_type is datetime:
			pass
		else:
			assert value == artifact_value

	for key, value in artifact_json.items():
		descriptor = getattr(Artifact, key)
		try:
			is_datetime = issubclass(descriptor.data_type, datetime)
		except TypeError:
			assert key in ['kill_chain', 'cef', 'tags', 'data']
		else:
			if is_datetime:
				if not value:
					value = None
				else:
					if not value.endswith('Z'):
						value += '00'

					value = datetime.strptime(value, descriptor.FORMAT)

		setattr(artifact, key, value)
		descriptor = getattr(Artifact, key)
		artifact_value = getattr(artifact, key)
		if descriptor.data_type is datetime:
			pass
		elif artifact_value is None:
			assert value is None
		else:
			assert value == artifact_value

		if artifact_value is not None:
			try:
				assert isinstance(artifact_value, descriptor.data_type)
			except TypeError:
				assert key in ['kill_chain', 'cef', 'tags', 'data']


def test_add_artifact(container: Container, artifact: Artifact):
	old_artifacts = list(container.artifacts)
	result = container.add_artifact(artifact)
	assert result is artifact
	new_artifacts = list(container.artifacts)
	assert artifact._data is  new_artifacts[-1]._data
	assert len(new_artifacts) == len(old_artifacts) + 1
