from pathlib import Path
from random import random
from tempfile import TemporaryDirectory
from unittest.mock import patch

from pytest import fixture, raises

from phantom_dev.dummy import MockedPhantomModule
with MockedPhantomModule():
	from phantom.base_connector import BaseConnector
	from phantom_dev import action_handler
	from phantom_dev.client.default_data.connector import (
		Connector as TestConnector)


ACTIONS = [
	{
		'identifier': 'test_connectivity',
		'param': {},
		'dict': {'status': action_handler.APP_SUCCESS, 'data': []},
	},
]


@fixture(params=ACTIONS)
def action(request):
	return request.param

@fixture
def action_identifier(action):
	return action['identifier']

@fixture
def param(action):
	return action['param']

@fixture
def result(action, param):
	action_dict = action['dict']
	result = action_handler.ActionResult(param)
	result.update_data(list(action_dict['data']))
	result.set_status(action_dict['status'])
	return result

@fixture
def connector(action_identifier):
	action_identifier_patch = patch.object(
		BaseConnector, 'get_action_identifier', lambda _: action_identifier)
	with TemporaryDirectory() as tmp_dir:
		instance = TestConnector()
		phantom_home = Path(tmp_dir, 'opt', 'phantom')
		phantom_home.mkdir(exist_ok=True, parents=True)
		phantom_home.joinpath('var', 'log', 'phantom').mkdir(
			parents=True, exist_ok=True)
		home_patch = patch.object(TestConnector, 'phantom_home', phantom_home)
		state_path = Path(tmp_dir, 'state')
		state_path.mkdir(exist_ok=True)
		state_patch = patch.object(
			BaseConnector, 'get_state_dir', lambda _: str(state_path))
		vault_path = Path(tmp_dir, 'vault')
		vault_path.mkdir(exist_ok=True)
		vault_patch = patch.object(
			action_handler.phantom.vault,
			'vault_info',
			lambda _: str(vault_path),
		)
		with state_patch, vault_patch, home_patch, action_identifier_patch:
			yield instance

@fixture
def log_message():
	return 'TEST LOG MESSAGE'


def test_logging(connector, log_message):
	connector.logger.debug(log_message)
	connector.logger.info(log_message)
	connector.logger.warning(log_message)
	connector.logger.error(log_message)
	connector.logger.critical(log_message)


def test_state(connector: TestConnector):
	with raises(AttributeError):
		connector.state

	with connector._persistent_state():
		old_state = connector.state

	with connector._persistent_state():
		new_state = connector.state
		assert new_state is not old_state
		assert new_state == old_state
		while new_state.get('random') == old_state.get('random'):
			new_state['random'] = random()

		random_value = new_state['random']

	with connector._persistent_state():
		final_state = connector.state
		assert final_state is not new_state
		assert final_state != old_state
		assert final_state == new_state
		assert final_state['random'] == random_value


def test_handle_action(connector: TestConnector, param, result):
	action_result = connector.handle_action(param)
	assert action_result.get_status() == result.get_status()
	assert action_result.get_data() == result.get_data()


def test_failure(connector):
	error = RuntimeError('test failure')
	def fake_method(*args): raise error
	with patch.object(connector, '_get_handler') as get_handler:
		get_handler.return_value.handler_method = fake_method
		with raises(RuntimeError) as exc_info:
			connector.handle_action(param)

		assert exc_info.value == error
